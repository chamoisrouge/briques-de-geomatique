..  _citation:

Citation de cette documentation
===================================

Cette documentation est distribuée sous licence CC BY-NC-SA 3.0 FR, ce qui la rend exploitable, utilisable et modifiable par tout le monde et pour tout contexte. Cependant, si vous l'utilisez pour votre apprentissage, votre recherche ou tout autre travail en général, n'hésitez pas à la citer. Une citation fait toujours plaisir !

Vous pouvez utiliser cette citation :

Passy, P. (2021). *Bienvenue sur les Briques de Géomatique !*. Briques de Géomatique. https://briques-de-geomatique.readthedocs.io/fr/latest/index.html

Ou utiliser cette :download:`entrée BibTex <_static/citation_briques_geomatique.bib>`
