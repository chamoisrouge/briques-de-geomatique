..  _rendu-carto:

Auteur : Paul Passy

Licence : |cc_by_nc_sa|

.. |cc_by_nc_sa| image:: figures/Cc-by-nc-sa_icone.png
              :width: 80 px

Rendu cartographique et visualisation
======================================

Le rendu cartographique est souvent un des produits finaux d'un projet géomatique faisant intervenir des SIG ou de la télédétection. Nous allons voir ici la façon de procéder pour réaliser quelques rendus cartographiques (et autres). Il ne s'agît pas de faire un cours de cartographie, nous verrons des rendus relativement simples. Reportez vous à des manuels de cartographie pure pour de plus amples détails et à des documentations spécialisées pour les traitements avancés.

.. contents:: Table des matières
    :local:

..  _visualisation_3D:

Visualisation 3D
------------------

Lorsque nous disposons d'une couche géographique possédant une valeur en *z* comme une valeur d'altitude, il est possible de créer une visualisation en 3D de nos données. C'est notamment le cas lorsque nous disposons d'un modèle numérique de terrain (MNT). Il est possible de visualiser le terrain comme si nous le survolions. Nous pouvons même y superposer des couches vecteurs ou des couches rasters pour mieux appréhender notre zone d'études.

.. note::
	Il est abusif de parler de 3D dans la plupart des cas. En toute rigueur, nous devrions parler de visualisation en 2.5D. En effet, un MNT reste un simple plan. Les pixels le constituant ne sont pas en trois dimensions. Nous parlons alors de 2.5D car la vue que nous créons s'apparente plus à une feuille de papier froissé qu'à un vrai pavé en trois dimensions.

Dans les exemples suivants, nous allons visualiser en 3D le `Mont Elbrouz`_, le plus haut sommet d'Europe, situé dans la chaîne du Causase en Russie. Le MNT utilisé sera le SRTM. Par dessus, nous y calquerons une composition colorée en vraies couleurs Landsat 8 de septembre 2020.

.. note::
	La visualisation 3D aide à mieux appréhender son terrain d'études et présente une sortie *sexy*. Mais d'un point de vue strictement scientifique, elle n'apporte pas grand chose.

.. contents:: Table des matières
    :local:

..  _visualisation_3D_QGIS2threejs:

Visualisation 3D dans QGIS avec l'extension QGIS2threejs
*********************************************************
Version de QGIS : 3.18.0

Il existe deux façons de créer des vues 3D dans QGIS. La plus aboutie est celle utilisant l'extension nommée QGIS2threejs. Cette extension est basée sur la technologie ̀ threejs`_. Il s'agît d'une extension Javascript permettant d'afficher et de manipuler des objets en 3D dans une page web. Ainsi, grâce à cette extension il sera possible de créer une vue 3D au sein de QGIS mais également d'exporter cette vue dans un fichier HTML. Ce type d'export nous permettra d'explorer et de partager notre vue avec un simple navigateur web sans avoir besoin de QGIS.

Pour commencer, il est nécessaire d'installer l'extension en allant dans le menu *Extensions* et en cherchant l'extension par son nom *QGIS2threejs*. Une fois installée, elle est accessible via le menu *Internet* > *QGIS2threejs*.

Nous chargeons maintenant notre MNT de la zone *SRTM_Elbrouz.tif*. Une fois ce raster chargé, nous ouvrons le menu principal de QGIS2threejs. La fenêtre principale de l'extension apparaît (:numref:`3d-threejs`). Dans le panneau *Layers*, dans le menu *DEM*, nous cochons notre MNT, à savoir *SRTM_Elbrouz*. Nous spécifions ainsi que le MNT qui sert de support à la 3d est ce raster ci.

.. figure:: figures/fen_threejs_init.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-threejs
    
    Initialisation de la vue 3D.

Le MNT apparaît avec la même symbologie que celle utilisée dans QGIS. Par défaut il s'agît d'un dégradé de gris, avec les altitudes les plus hautes qui tendent vers le blanc et les plus basses vers le noir. En allant dans le menu *Widget*, il est possible d'afficher le repère x, y, et z (*Navigation Widget*) en bas à droite et la flèche du nord (*North Arrow*).

Il est possible de régler l’exagération verticale, i.e. de l'altitude. Par défaut, il n'y en a pas, elle est à *1*. Sur l'exemple précédent, elle est réglée à *2*, ce qui permet d'accentuer le relief. Pour cela, nous allons dans le menu *Scene* > *Scene Settings...* et dans le menu qui s'ouvre nous pouvons mettre la *Vertical exaggeration* à *2*.

Superposer un raster par dessus est très simple. Il suffit de charger le raster d'intérêt, ici *LC08_20200927_Elbrouz.tif*, dans QGIS et de le mettre au-dessus du raster dans le panneau de couches. La vue 3D se met à jour automatiquement. Notons, que si nous disposions de fichiers vecteurs, il serait possible de les ajouter de la même manière. Nous pourrions également gérer leur affichage via les menus *Line*, *Polygon* et *Point Cloud* du panneau *Layers* (:numref:`3d-threejs-landsat`).

.. figure:: figures/fen_threejs_Landsat.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-threejs-landsat
    
    Vue 3D avec une composition colorée Landsat superposée.

Il est possible de zoomer et de dézoomer dans cette vue 3D à l'aide de la molette de la souris. Le changement d'inclinaison et d'orientation de la vue se fait à l'aide du curseur de la souris. Pour changer le centre de la vue, cela se fait également via le curseur de la souris mais en maintenant enfoncée la touche *Control*. Lors des mouvements, la flèche du Nord pointe toujours vers le nord et le repère de navigation suit le mouvement général (:numref:`3d-threejs-orientation`).

.. figure:: figures/fen_threejs_orientation.png
    :width: 45em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-threejs-orientation
    
    Changement de position de la vue 3D.

Il est possible d'exporter votre vue 3D en image PNG via le menu *File* > *Save scene as...* > *Image (.png)*. Il est également possible d'exporter votre vue 3D en un vrai fichier 3D en choisissant le format *glTF (.gltf, .glb)*. Ce format est à la 3D ce que le Jpeg est à la 2D. Vous pourrez par exemple importer votre vue dans un logiciel de 3D comme Blender.

Une force de cette extension est de pouvoir exporter les vues 3D dans un fichier HTML manipulable facilement avec n'importe quel navigateur web. Pour cela, il suffit d'aller dans *File* > *Export to Web...* La fenêtre d'export s'ouvre alors (:numref:`3d-threejs-export`).

.. figure:: figures/fen_threejs_export.png
    :width: 28em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-threejs-export
    
    Export de la vue 3D pour le Web.

Dans le champ *Output Directory* nous spécifions un répertoire d'export. Dans le champ *HTML filename*, nous indiquons un nom pour le fichier HTML principal, ici *Elbrouz.html*. Nous pouvons indiquer un titre pour la page Web dans le champ *Page Title*. Il est conseillé de cocher la case *Enable the Viewer to Run Locally* pour éviter certains bugs d'affichage. Puis nous cliquons sur *Export*.

Dans notre explorateur de fichier, nous n'avons plus qu'à ouvrir le fichier *Elbrouz.html* avec un navigateur pour explorer notre vue 3D indépendamment de QGIS.

.. note::
	**Au final**, cette solution est vraiment très paratique. Le rendu est bon, fluide et rapide. L'intégration dans QGIS est très bonne et la navigation dans la vue 3D facile. La possibilité d'exporter en HTML est également très appréciable, je pense que c'est la meilleure solution pour de la visualisation 3D dans QGIS.

..  _visualisation_3D_qgis_carto:

Visualisation avec la Vue Cartographique 3D de QGIS
*********************************************************
Version de QGIS : 3.18.0

Il existe dans QGIS, une façon native de visualiser les données en 3D sans passer par des extensions tierces. Cette visualisation se fait à l'aide de la *Vue Cartographique 3D*.

Une fois notre MNT chargé, nous pouvons ouvrir cette vue 3D via le menu *Vue* > *Nouvelle Vue Cartographique 3D*. La vue fenêtre de la vue 3D s'affiche alors. C'est une fenêtre flottante qu'il est possible "d'accrocher" à un des bords de la fenêtre principale de QGIS (:numref:`3d-qgis`).

.. figure:: figures/fen_vue_3D_qgis.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-qgis
    
    Vue Cartographique 3D de QGIS.

La première chose à faire est de définir le MNT qui va servir de support à la 3D. Pour cela, nous allons dans le menu *Options* sous l'icône |icone_options| et nous sélectionnons *Configurer* (:numref:`3d-qgis-options`).

.. figure:: figures/fen_vue_3D_qgis_options.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-qgis-options
    
    Réglage des options de la vue 3D dans QGIS.

Dans le champ *Type* nous spécifions que nous souhaitons représenter un *MNT (couche Raster)*. Dans le champ *Élévation*, nous indiquons le MNT à représenter, à savoir *SRTM_Elbrouz*. Nous pouvons régler l'échelle verticale, qui est en fait l'exagération verticale. Nous pouvons la mettre à *2*. Puis nous cliquons sur *OK*.

Le MNT est vu d'en haut, il faut maintenant régler l'angle de vue pour profiter de la 3D. Pour cela, nous cliquons n'importe où sur la vue 3D et nous bougeons le curseur de la souris en maintenant la touche *Majuscule* enfoncée. Il est aussi possible de zoomer et dézoomer via la molette de la souris. Nous pouvons également déplacer la vue avec la souris comme nous le ferions du raster dans la fenêtre principale de QGIS (:numref:`3d-qgis-orientation`).

.. figure:: figures/fen_vue_3D_qgis_orientation.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-qgis-orientation
    
    Réglage de l'angle de vue.

La superposition d'une autre couche se fait simplement en mettant une autre couche par dessus le MNT dans le panneau des couches dans QGIS. En ce qui concerne l'affichage des couches, la vue 3D se comporte comme la vue normale de QGIS. Ici, nous superposons la composition colorée Landsat 8 de septembre 2020 *LC08_20200927_Elbrouz.tif* (:numref:`3d-qgis-landsat`).

.. figure:: figures/fen_vue_3D_qgis_Landsat.png
    :width: 30em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-qgis-landsat
    
    Superposition d'une composition colorée vraies couleurs Landsat 8.

Il est possible d'exporter une image de sa vue 3D via l'icône |icone_export_image| *Enregistrer comme image...*. Nous pouvons également exporter notre vue en objet 3D *.obj* manipulable par d'autres logiciels en cliquant sur l'icône |icone_export_objet| *Export 3D Scene*.

..  _visualisation_3D_video:

Créer une animation ou une vidéo
**********************************
Il est possible d'exporter une série de vues au format Jpeg afin de les assembler dans un GIF animé ou dans une vidéo. Au final nous obtenons une vidéo d'un survol de notre vue 3D. Pour cela nous ouvrons le panneau d'animation en cliquant sur l'icône *Animations* |icone_export_animation|. Le panneau d'animation s'ouvre en bas de la fenêtre. Par défaut, la vidéo débutera avec la vue qui apparaît au moment de l'ouverture du menu d'animations.

Nous changeons maintenant d'angle de vue, à l'aide du curseur de la souris, pour avoir notre deuxième vue de la vidéo. Lorsque nous somme satisfaits de notre deuxième vue, nous cliquons sur *Ajouter une image clé* |icone_export_add|. Une fenêtre nous demande la position temporelle de cette vue dans l'animation, ici nous indiquons que cette vue doit apparaître après *1* seconde de vidéo (:numref:`3d-frame`).

.. figure:: figures/fen_export_3D_video_frame.png
    :width: 14em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-frame
    
    Position temporelle de la vue.

Nous répétons ensuite cette opération avec autant d'étapes que désirées. Par défaut, la vidéo dure 5 secondes, il est possible de faire une vue toutes les secondes. Ainsi, nous répétons cette étape avec des vues différentes aux positions 2, 3 et 4 secondes. La position de la seconde 5 est déjà fixée par défaut. Mais à l'aide de l'édition des images clés, il est possible de la modifier.

Il faut bien noter, que ce menu d'animations fera des interpolations d'images entre nos différentes vue afin d'obtenir une vidéo fluide. Il est possible de régler la nature de l'interpolation dans le menu *Interpolation*. Avant d'exporter notre vidéo, nous pouvons la jouer pour en juger la qualité. Il suffit de cliquer sur l'icône |icone_export_animation| en bas. Si nous sommes satisfaits, nous exportons les images de l'animation en cliquant sur *Exporter les images de l'animation* |icone_enregistrer|. Il faut bien noter, que nous n'exportons pas la vidéo directement mais seulement les images la constituant. C'est pourquoi la fenêtre d'export des images s'affiche (:numref:`3d-export-images`).

.. figure:: figures/fen_export_3D_images.png
    :width: 23em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: 3d-export-images
    
    Paramètres d'export des images de l'animation.

Nous ne touchons pas au champ *Modèle*. Nous sélectionnons juste un *Répertoire de destination*. Les autres réglages peuvent être laissés à leurs valeurs par défaut. Puis nous cliquons sur *OK*.

.. note::
	Changer la nature de l'interpolation des images ne semble pas changer drastiquement le rendu.

Si nous regardons dans notre explorateur de fichiers le répertoire créé, nous constatons que plusieurs dizaines d'images *.jpg* ont été exportées. Seules 5 de ces images correspondent aux vues que nous avons définies, les autres résultent de l'interpolation. C'est maintenant à l'utilisateur de créer son animation à partir de ces images. Il est possible de créer un GIF animé, pour les nostalgiques des années 90, ou un fichier vidéo en format *.avi*, *.mp4* ou autre.

Pour un GIF animé, il est possible d'utiliser une solution en ligne ou le logiciel  ̀ImageMagick`_ (disponible sous Linux et Windows) qui s'utilise en lignes de commandes. Une fois ce logiciel installé, nous nous positionnons dans le répertoire contenant les images et nous y ouvrons une invite de commandes. Nous pouvons entrer la commande suivante.

.. code-block:: sh

   convert *.jpg anim.gif

*convert* est le mot clé de ImageMagick pour convertir le format des images, ici de *.jpg* à *.gif*. *\*.jpg* signifie que nous prenons toutes les images du *Jpeg* du répertoire. Et *anim.gif* signifie que nous les assemblons dans un fichier GIF que nous nommons *anim*. Au bout de quelques secondes, nous obtenons notre GIF animé.

Pour créer une vidéo, nous pouvons utiliser l'utilitaire `ffmpeg`_ qui s'utilise également en ligne de commandes. Comme précédemment, nous nous positionnons dans le répertoire contenant les images, nous ouvrons une invite de commandes et nous pouvons y entrer la commande suivante.

.. code-block:: sh

   ffmpeg -framerate 25 -pattern_type glob -i '*.jpg' -c:v libx264 Elbrouz.mp4

Pour les intéressés par les options de *ffmpeg* regardez la documentation sinon acceptons la commande telle quelle. Nous obtenons ici une vidéo *.mp4* nommée *Elbrouz.mp4*. Notons qu'elle pèse bien moins lourd que le GIF animé.

.. raw:: html
   
   <video width="400" controls>
   <source src="_static//Elbrouz.mp4" type="video/mp4">
   Sorry, your browser doesn't support embedded videos.
   </video> 

..  _synchroniser_vues:

Synchroniser des vues
-----------------------

En géomatique, et en télédétection en particulier, il est souvent utile de comparer visuellement un même territoire avec différentes sources de données. Par exemple, pour réaliser une classification supervisée, il est pratique de comparer facilement une composition colorée (:ref:`composition-coloree`) de l'image à classifier avec une vue à plus haute résolution afin de digitaliser des zones d'entraînement pertinentes. Une synchronisation des vues est également utile pour suivre un territoire dans le temps et comparer une vue récente avec une vue historique.

..  _synchroniser_vues_qgis:

Synchroniser des vues dans QGIS
*********************************
Version de QGIS : 3.22.3

QGIS propose une solution permettant de synchroniser plusieurs vues. Dans l'exemple ci-après, nous allons synchroniser une vue contenant une composition colorée fausses couleurs d'une zone du `Vexin français`_, avec une vue Google Satellite à très haute résolution spatiale et une vue contenant la Carte d'État-Major du 19ème siècle fournie par l'IGN.

Nous commençons par charger dans QGIS les données dont nous aurons besoin pour les trois vues. Les fonds Google Satellite et IGN historique sont accessibles par flux WMS (:ref:`WMS`). Une fois les trois couches chargées, nous réglons la vue principale qui ne contiendra que la composition colorée Sentinel-2 de la zone. Une fois la composition colorée réglée, nous décochons les deux autres couches afin de n'avoir que l'image Sentinel-2 d'afficher (:numref:`couches_synchro`).

.. figure:: figures/fig_qgis_couches_pour_synchro.png
    :width: 23em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: couches_synchro

    Réglage de la couche à utiliser en vue principale.

Une fois satisfait de la vue, il est nécessaire de l'enregistrer sous forme de *thème*. Pour cela, nous cliquons sur l'icône |icone_themes| ``Gérer les thèmes de la carte`` qui se trouve en haut du panneau des couches. Dans le menu déroulant qui apparait, nous choisissons ``Ajouter un thème...`` que nous pouvons nommer *Compo Col S2*. Ce thème apparaît maintenant dans la liste des thèmes accessibles via l'icône |icone_themes|. Un fois le premier thème réglé, nous réglons le deuxième qui ne contiendra que la vue Google Satellite. Nous décochons tout sauf la couche Google Satellite et nous nommons ce thème *Google Sat* en procédant comme pour la vue de la composition colorée. Enfin, nous procédons de même pour la troisième vue contenant la carte d'État-Major. Nous pouvons nommer ce troisième thème *Carte EM*. Nous disposons maintenant de nos trois thèmes dans la liste déroulante accessible via l'icône |icone_themes| (:numref:`liste_themes`). Nous n'oublions pas de laisser notre vue principale sur la composition colorée.

.. figure:: figures/fig_qgis_liste_themes.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: liste_themes

    Les trois thèmes paramétrés.

L'étape suivante consiste à ajouter deux nouvelles vues cartographiques contenant respectivement le fond Google Satellite et la carte historique. Pour cela, nous allons dans le menu :menuselection:`Vue --> Nouvelle vue cartographique`. Une nouvelle vue nommée *Carte 1* apparaît à l'écran. Nous allons lui faire suivre le thème *Google Sat*. Pour cela, en haut de cette vue *Carte 1*, nous cliquons sur l'icône |icone_themes| et nous sélectionnons le thème *Google Sat*. Afin de faciliter la comparaison entre les deux vues, nous allons synchroniser le centre des deux vues et les échelles. Pour cela nous cliquons sur l'icône |icone_reglages| ``Paramètres de la vue`` et nous sélectionnons ``Synchroniser le Centre de la Vue avec la Carte Principale`` et ``Synchroniser l'échelle``. Maintenant, lorsque nous déplaçons ou zoomons à partie d'une des deux vues, l'autre vue suit. De plus, lorsque nous passons le curseur sur la vue principale, sa position apparaît également sur la vue secondaire (:numref:`synchro_curseurs`).

.. figure:: figures/fig_qgis_synchro_curseurs.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: synchro_curseurs

    Synchronisation des vues et des curseurs. Sur la vue Google satellite (à droite), le point blanc dans le cercle rouge correspond à la position du curseur sur la vue principale (à gauche).

Selon le même principe, nous ajoutons une troisième vue, à laquelle nous associons le thème *Carte Em*. Nous procédons aux mêmes réglages de synchronisation des centres de vues et des échelles. Les trois vues sont maintenant synchronisées et comparables. Il est possible d'explorer les éléments inchangés du paysage tout en profitant de l'apport du multi-spectral des images Sentinel-2 (:numref:`synchro_3_vues`).

.. figure:: figures/fig_qgis_synchro_3_vues.png
    :width: 35em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: synchro_3_vues

    Synchronisation des trois vues.

.. note::
	Il n'y a pas de limites dans le nombre de vues synchronisées possibles, tout dépend de la taille de votre écran. Il est possible de désolidariser les vues secondaires de QGIS afin de les avoir sur des fenêtres autonomes. Il suffit de cliquer sur l'icône |icone_dock-vues| ``Dock 2D Map View`` se trouvant en haut des vues. Cette option peut être pratique lorsque nous disposons de plusieurs écrans.


..  _affichage_R:

Affichage de couches avec R
-----------------------------
Version de R : 4.3.2

Version de terra : 1.7.29

Nous verrons ici comment afficher, en les superposant, plusieurs couches géographiques dans R en utilisant la librairie *terra*. Dans cet exemple, nous mettrons en fond un MNT du bassin de la Roya afin d'avoir la topographie de la zone, puis nous y superposerons les limites du bassin ainsi que le réseau hydrographique. Nous verrons comment régler les couleurs et d'autres paramètres comme le titre de la légende. Il est possible d'ajouter facilement une flèche du nord ainsi qu'une barre d'échelle. Le rendu n'est pas tout à fait une carte car toutes les couches ne sont pas dans la légende mais ce genre d'illustrations peut suffire dans certains cas.

.. code-block:: R

    # import de MNT de la Roya
    mnt <- terra::rast('./srtm_roya_L93.tif')
    # import des limites du bassin de la Roya
    lim_bv <- terra::vect('./hydro_bv_Roya_L93.gpkg')
    # import du réseau hydro de la Roya
    hydro <- terra::vect('./hydro_Roya_L93.gpkg')

    # affichage du MNT en teintes de gris avec un titre
    terra::plot(mnt, col=gray.colors(100), main='Topographie de la Roya',
                plg=list( # paramètres pour la légende
                  title = "Altitude (m)",
                  title.cex = 1.2, # taille du titre de la légende
                  cex = 1 # taille du texte de la légende
                ))
    # ajout des limites du bassin en rouge et de largeur 2
    terra::lines(lim_bv, col='red', lwd=2)
    # ajout du réseau hydro en bleu
    terra::lines(hydro, col='blue')
    # ajout d'une flèche du nord
    terra::north()
    # ajout d'une barre d'échelle
    terra::sbar()

Nous obtenons la représentation suivante (:numref:`terra_plot`).

.. figure:: figures/fig_R_terra_plot.png
    :width: 25em
    :align: center
    :alt: alternate text
    :figclass: align-center
    :name: terra_plot

    Représentation cartographique avec terra.

.. note::
    La superposition des couches fonctionne même si elles ne sont pas toutes dans le même système de coordonnées de référence. Le SCR de la première couche affichée sera alors utilisée.

.. |icone_enregistrer| image:: figures/icone_3D_enregistrer.png
              :width: 17 px

.. |icone_export_add| image:: figures/icone_export_3D_add_frame.png
              :width: 17 px


.. |icone_options| image:: figures/icone_3d_options.png
              :width: 25 px

.. |icone_export_image| image:: figures/icone_export_3D_image.png
              :width: 20 px

.. |icone_export_objet| image:: figures/icone_export_3D_obj.png
              :width: 20 px

.. |icone_export_animation| image:: figures/icone_export_3D_animation.png
              :width: 17 px

.. |icone_themes| image:: figures/icone_qgis_themes.png
              :width: 20 px

.. |icone_reglages| image:: figures/icone_qgis_reglages.png
              :width: 17 px

.. |icone_dock-vues| image:: figures/icone_qgis_dock_vue.png
              :width: 20 px


.. _ImageMagick: https://imagemagick.org/index.php
.. _threejs: https://threejs.org/
.. _Mont Elbrouz: https://fr.wikipedia.org/wiki/Elbrouz
.. _ffmpeg: https://ffmpeg.org/
.. _Vexin français: https://fr.wikipedia.org/wiki/Vexin_fran%C3%A7ais

