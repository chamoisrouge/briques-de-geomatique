<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis hasScaleBasedVisibilityFlag="0" version="3.4.4-Madeira" styleCategories="AllStyleCategories" minScale="1e+8" maxScale="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property value="false" key="WMSBackgroundLayer"/>
    <property value="false" key="WMSPublishDataSourceUrl"/>
    <property value="0" key="embeddedWidgets/count"/>
    <property value="Value" key="identify/format"/>
  </customproperties>
  <pipe>
    <rasterrenderer band="1" opacity="1" alphaBand="-1" type="paletted">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
        <paletteEntry color="#ff5500" value="11" label="Cultures d'été" alpha="255"/>
        <paletteEntry color="#ffff7f" value="12" label="Cultures d'hiver" alpha="255"/>
        <paletteEntry color="#009c00" value="31" label="Forêt de feuillus" alpha="255"/>
        <paletteEntry color="#003200" value="32" label="Forêt de conifères" alpha="255"/>
        <paletteEntry color="#aaff00" value="34" label="Pelouses" alpha="255"/>
        <paletteEntry color="#55aa7f" value="36" label="Landes ligneuses" alpha="255"/>
        <paletteEntry color="#ff00ff" value="41" label="Urbain dense" alpha="255"/>
        <paletteEntry color="#ff55ff" value="42" label="Urbain diffus" alpha="255"/>
        <paletteEntry color="#ffaaff" value="43" label="Zones industrielles et commerciales" alpha="255"/>
        <paletteEntry color="#00ffff" value="44" label="Routes" alpha="255"/>
        <paletteEntry color="#ff0000" value="45" label="Surfaces minérales" alpha="255"/>
        <paletteEntry color="#ffb802" value="46" label="Plages et dunes" alpha="255"/>
        <paletteEntry color="#0000ff" value="51" label="Eau" alpha="255"/>
        <paletteEntry color="#bebebe" value="53" label="Glaciers ou neige" alpha="255"/>
        <paletteEntry color="#aaaa00" value="211" label="Prairies" alpha="255"/>
        <paletteEntry color="#aaaaff" value="221" label="Vergers" alpha="255"/>
        <paletteEntry color="#550000" value="222" label="Vignes" alpha="255"/>
      </colorPalette>
      <colorramp name="[source]" type="randomcolors"/>
    </rasterrenderer>
    <brightnesscontrast brightness="0" contrast="0"/>
    <huesaturation saturation="0" colorizeRed="255" grayscaleMode="0" colorizeStrength="100" colorizeGreen="128" colorizeOn="0" colorizeBlue="128"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
