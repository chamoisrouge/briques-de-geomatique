<!DOCTYPE qgis PUBLIC 'http://mrcc.com/qgis.dtd' 'SYSTEM'>
<qgis maxScale="0" styleCategories="AllStyleCategories" version="3.4.4-Madeira" minScale="1e+8" hasScaleBasedVisibilityFlag="0">
  <flags>
    <Identifiable>1</Identifiable>
    <Removable>1</Removable>
    <Searchable>1</Searchable>
  </flags>
  <customproperties>
    <property key="WMSBackgroundLayer" value="false"/>
    <property key="WMSPublishDataSourceUrl" value="false"/>
    <property key="embeddedWidgets/count" value="0"/>
    <property key="identify/format" value="Value"/>
  </customproperties>
  <pipe>
    <rasterrenderer type="paletted" alphaBand="-1" opacity="1" band="1">
      <rasterTransparency/>
      <minMaxOrigin>
        <limits>None</limits>
        <extent>WholeRaster</extent>
        <statAccuracy>Estimated</statAccuracy>
        <cumulativeCutLower>0.02</cumulativeCutLower>
        <cumulativeCutUpper>0.98</cumulativeCutUpper>
        <stdDevFactor>2</stdDevFactor>
      </minMaxOrigin>
      <colorPalette>
        <paletteEntry color="#ff5500" value="11" label="Cultures d'été" alpha="255"/>
        <paletteEntry color="#ffff7f" value="12" label="Cultures d'hiver" alpha="255"/>
        <paletteEntry color="#009c00" value="31" label="Forêt de feuillus" alpha="255"/>
        <paletteEntry color="#003200" value="32" label="Forêt de conifères" alpha="255"/>
        <paletteEntry color="#aaff00" value="34" label="Pelouses" alpha="255"/>
        <paletteEntry color="#55aa7f" value="36" label="Landes ligneuses" alpha="255"/>
        <paletteEntry color="#ff00ff" value="41" label="Urbain dense" alpha="255"/>
        <paletteEntry color="#ff55ff" value="42" label="Urbain diffus" alpha="255"/>
        <paletteEntry color="#ffaaff" value="43" label="Zones industrielles et commerciales" alpha="255"/>
        <paletteEntry color="#00ffff" value="44" label="Routes" alpha="255"/>
        <paletteEntry color="#ff0000" value="45" label="Surfaces minérales" alpha="255"/>
        <paletteEntry color="#ffb802" value="46" label="Plages et dunes" alpha="255"/>
        <paletteEntry color="#0000ff" value="51" label="Eau" alpha="255"/>
        <paletteEntry color="#bebebe" value="53" label="Glaciers ou neige" alpha="255"/>
        <paletteEntry color="#aaaa00" value="211" label="Prairies" alpha="255"/>
        <paletteEntry color="#aaaaff" value="221" label="Vergers" alpha="255"/>
        <paletteEntry color="#550000" value="222" label="Vignes" alpha="255"/>
      </colorPalette>
      <colorramp type="randomcolors" name="[source]"/>
    </rasterrenderer>
    <brightnesscontrast contrast="0" brightness="0"/>
    <huesaturation colorizeBlue="128" colorizeOn="0" colorizeRed="255" colorizeGreen="128" saturation="0" grayscaleMode="0" colorizeStrength="100"/>
    <rasterresampler maxOversampling="2"/>
  </pipe>
  <blendMode>0</blendMode>
</qgis>
